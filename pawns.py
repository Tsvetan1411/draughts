class Pawn:
    moves = {"av_moves": [], "av_captures": {}}
    def __init__(self, color, current_position, is_dame, is_player):
        self.color = color
        self.dame = is_dame
        self.current_position = current_position
        self.is_player = is_player

        if color == 'black':
            self.symbol = "X"
            self.dame_symbol = "K"
        elif color == "white":
            self.symbol = "O"
            self.dame_symbol = "Q"

        if is_player:
            self.move_directions = [(1, -1), (-1, -1)]
        else:
            self.move_directions = [(1, 1), (-1, 1)]
        
        
    def __repr__(self):
        return self.symbol if self.dame == False else self.dame_symbol
    
    def search_for_moves_piece(self, board_dict):
        self.moves = {"av_moves": [], "av_captures": {}}
        opponent_color = "white" if self.color == "black" else "black"
        for x, y in self.move_directions:
                self_x = ord(self.current_position[0])
                self_y = int(self.current_position[1])
                next_field = chr(self_x + x) + str(self_y + y)

                if next_field not in board_dict:
                    continue
                elif board_dict[next_field] == " ":
                    self.moves["av_moves"].append(next_field)                    
                elif board_dict[next_field].color == opponent_color:
                    mandatory_next_field = chr(self_x + x*2) + str(self_y + y*2)
                    if mandatory_next_field in board_dict:
                        if board_dict[mandatory_next_field] == " ": 
                            self.moves["av_captures"].setdefault(next_field, []).append(mandatory_next_field)

    def search_for_moves_dame(self, board_dict):
        self.moves = {"av_moves": [], "av_captures": {}}
        opponent_color = "white" if self.color == "black" else "black"
        move_directions = [(1, -1), (1, 1), (-1, -1), (-1, 1)]

        for x, y in move_directions:
                self_x = ord(self.current_position[0])
                self_y = int(self.current_position[1])
                meet_opponent = None
                while True:
                    self_x += x 
                    self_y += y
                    next_field = chr(self_x) + str(self_y)
                    
                    if next_field not in board_dict:
                        break

                    next_field_color = "none" if not hasattr(board_dict[next_field], "color" ) else board_dict[next_field].color

                    if next_field_color == self.color:
                        break
                    elif meet_opponent and next_field_color == opponent_color:
                        break
                    elif not meet_opponent and board_dict[next_field] == " ":
                        self.moves["av_moves"].append(next_field)
                    elif next_field_color == opponent_color and not meet_opponent:
                        mandatory_next_field = chr(self_x + x) + str(self_y + y)
                        if mandatory_next_field not in board_dict:
                            break
                        elif board_dict[mandatory_next_field] == " ":
                            meet_opponent = next_field
                            self.moves["av_captures"].setdefault(meet_opponent, [])
                        else:
                            break
                    elif meet_opponent and board_dict[next_field] == " ":
                        self.moves["av_captures"][meet_opponent].append(next_field)

    def movement(self, board_dict, mandatory_captures, cpu_input=None):
        player_input = str(input("Pole docelowe: ")).lower() if cpu_input == None else cpu_input
        made_move = False

        if player_input not in board_dict:
            print("Błędne polecenie")
        elif self.current_position in mandatory_captures:
            for opponent, fields in self.moves["av_captures"].items():
                if player_input in fields:
                    board_dict[player_input] = self
                    board_dict[opponent] = " "
                    board_dict[self.current_position] = " "
                    self.current_position = player_input
                    made_move = True
                else:
                    print("Błędne polecenie")
        elif player_input in self.moves["av_moves"]:
            board_dict[player_input] = self
            board_dict[self.current_position] = " "
            self.current_position = player_input
            made_move = True
        else:
            print("Błędne polecenie")

        return board_dict, made_move
        
def setup_random_pawns(board_dict, player_color):
        board_list = list(board_dict)
        if player_color == "black":
            cpu_color = "white"
        else:
            cpu_color = "black"
        for num in range(0, 24):
            if (ord(board_list[num][0]) + int(board_list[num][1])) % 2 == 0:
                board_dict[board_list[num]] = Pawn(cpu_color, board_list[num], False, False)
        for num in range(40, 64):
            if (ord(board_list[num][0]) + int(board_list[num][1])) % 2 == 0:
                board_dict[board_list[num]] = Pawn(player_color, board_list[num], False, True)
        return board_dict
