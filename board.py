import pawns

class Board:
    def __init__(self):
        self.board_dict = {}
        self.letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
        for number in range(1, 9):
            for letter in self.letters:
                key = letter + str(number)
                self.board_dict[key] = " "

    def display(self):
        print("  " + " ".join(self.letters))
        for number in range(1, 9):
            row = str(number) + " "
            for letter in self.letters:
                key = letter + str(number)
                row += str(self.board_dict[key]) + " "
            print(row)
        print("*" * 18)

    def moves_serch_initialization(self):
        for field in self.board_dict:
            if isinstance(self.board_dict[field], pawns.Pawn):
                if self.board_dict[field].dame == True:
                    self.board_dict[field].search_for_moves_dame(self.board_dict)
                else:
                    self.board_dict[field].search_for_moves_piece(self.board_dict)

    def change_to_dame(self):
        for field in self.board_dict:
            if isinstance(self.board_dict[field], pawns.Pawn):
                if self.board_dict[field].current_position[1] == "1" and self.board_dict[field].is_player == True:
                    self.board_dict[field].dame = True
                elif self.board_dict[field].current_position[1] == "8" and self.board_dict[field].is_player == False:
                    self.board_dict[field].dame = True

    def mandatory_captures(self):
        mc_for_player = []
        mc_for_cpu = []
        for field in self.board_dict:
            if isinstance(self.board_dict[field], pawns.Pawn):
                if self.board_dict[field].moves["av_captures"]:
                    if self.board_dict[field].is_player:
                        mc_for_player.append(field)
                    else:
                        mc_for_cpu.append(field)
        return mc_for_player, mc_for_cpu

    def check_for_loose(self, player_color):
        av_moves = False
        for field in self.board_dict:
            if isinstance(self.board_dict[field], pawns.Pawn):
                if self.board_dict[field].color == player_color:
                    if self.board_dict[field].moves["av_moves"] or self.board_dict[field].moves["av_captures"]:
                        av_moves = True
        return av_moves

