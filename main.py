import pawns
import board
import opponent
from random import randint

def play():
    colors = ["white", "black"]
    player_color = colors[randint(0, 1)]
    cpu_color = colors[0] if player_color == colors[1] else colors[1]
    cpu = opponent.Opponent(cpu_color)
    new_board = board.Board()
    new_board.board_dict = pawns.setup_random_pawns(new_board.board_dict, player_color)
    
    player_turn = True if player_color == "white" else False

    while True:
        new_board.change_to_dame()
        new_board.moves_serch_initialization()
        mc_player, mc_cpu =  new_board.mandatory_captures()
        new_board.display()

        if player_turn:
            made_move = False

            if not new_board.check_for_loose(player_color):
                print("Przegrałeś!")
                break

            while not made_move:
                player_input = str(input("Wybierz figurę: ")).lower()
                try:
                    if isinstance(new_board.board_dict[player_input], pawns.Pawn) and new_board.board_dict[player_input].is_player == True:
                        if mc_player and player_input not in mc_player:
                            print("Masz obowiązkowe bicia!")
                        else:
                            new_board.board_dict, made_move = new_board.board_dict[player_input].movement(new_board.board_dict, mc_player)
                    else:
                        print("Błędne polecenie")
                except(KeyError):
                    print("Błędne polecenie")
       
            if made_move:
                player_turn = False
        else:
            if not new_board.check_for_loose(cpu_color):
                print("Wygrałeś!")
                break
            pawn, move = cpu.moves(mc_cpu, new_board.board_dict)
            new_board.board_dict[pawn].movement(new_board.board_dict, mc_cpu, move)
            print(f"Przeciwnik ruszył się z {pawn} na {move}")
            player_turn = True

play()